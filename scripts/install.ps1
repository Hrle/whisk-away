#!/usr/bin/env pwsh

$currentRole = [Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()
$adminRole = [Security.Principal.WindowsBuiltInRole] "Administrator"
$isAdmin = $currentRole.IsInRole($adminRole)
if (-NOT $isAdmin) {
  $elevetedArgs = "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`""
  Start-Process pwsh -ArgumentList $elevetedArgs -Verb RunAs
  Exit
}

cargo build --release

$programFilesPath = [Environment]::GetFolderPath("ProgramFiles")
$whiskAwayPath = Join-Path $programFilesPath "Whisk Away"
if (!(Test-Path -Path $whiskAwayPath)) {
  New-Item -ItemType Directory -Path $whiskAwayPath
}
Write-Host "Created '$whiskAwayPath'."

$root = Resolve-Path (Join-Path $PSScriptRoot "..")

$bin = Join-Path $root "target\release\whisk-away.exe"
$binDestinationPath = Join-Path $whiskAwayPath "whisk-away.exe"
Copy-Item -Path $bin -Destination $binDestinationPath -Force
Write-Host "Whisk Away binary copied to '$binDestinationPath'."

$assets = Join-Path $root "assets"
$assetsDestinationPath = Join-Path $whiskAwayPath "assets"
Copy-Item -Path $assets -Destination $assetsDestinationPath -Force
Write-Host "Whisk Away assets copied to '$assetsDestinationPath'."

$startMenuPath = [Environment]::GetFolderPath("CommonPrograms")
$startMenuFolder = Join-Path $startMenuPath "Whisk Away"
if (!(Test-Path -Path $startMenuFolder)) {
  New-Item -ItemType Directory -Path $startMenuFolder
}
$shortcutPath = Join-Path $startMenuFolder "Whisk Away.lnk"
$workingDirectory = [Environment]::GetFolderPath("UserProfile")
$wshShell = New-Object -ComObject Wscript.Shell
$shortcut = $wshShell.CreateShortcut($shortcutPath)
$shortcut.TargetPath = "$destinationPath"
$shortcut.Description = "Whisk Away"
$shortcut.WorkingDirectory = $workingDirectory
$shortcut.WindowStyle = 7
$shortcut.Save()
Write-Host "Whisk Away start menu shortcut created at '$shortcutPath'."

$startupPath = [Environment]::GetFolderPath("CommonStartup")
$startupShortcutPath = Join-Path $startupPath "Whisk Away.lnk"
Copy-Item -Path $shortcutPath -Destination $startupShortcutPath -Force
Write-Host "Whisk Away startup shortcut created at '$startupShortcutPath'."

Write-Host "Whisk Away installed successfully."
Write-Host -NoNewLine 'Press any key to continue...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
