<p align="center">
    <img alt="Logo" src="./assets/logo-256x256.png" />
</p>

# Whisk Away

Whisk Away is a simple, intuitive, and efficient file watcher service written
in Rust. It's designed to help you manage your Downloads directory by
automatically moving files to a designated location.

## About

In our day-to-day digital lives, we often download various types of files which
end up cluttering our Downloads directory. Finding a specific file in this
chaotic directory can be a hassle. Here is where Whisk Away comes to the
rescue!

Whisk Away is a background service that watches your Downloads directory and
automatically moves files to a user-specified location. This way, you can keep
your Downloads directory clean and organized without lifting a finger.

## Features

- Watches your Downloads directory for any new files.
- Automatically moves new files to a specified location.

This is a simple version of what we hope Whisk Away can become. Future
extensions of Whisk Away may include file categorization, integration with
cloud storage, file compression, logs, and user notifications.

## Install

### Windows

**Prerequisites**: You need to have Rust and Cargo installed on your system to
build the `whisk-away` executable.

1. Clone the `whisk-away` repository to your local machine:

    ```pwsh
    git clone https://github.com/hrle/whisk-away
    cd whisk-away
    ```

    Open a PowerShell terminal with administrative privileges. To do this,
    search for "PowerShell" in the Start menu, right-click on
    "Windows PowerShell," and choose "Run as administrator."

2. Run the install script:

    ```pwsh
    powershell.exe -ExecutionPolicy Bypass -File .\scripts\install.ps1
    ```

    The script will build the whisk-away executable, install it in
    `Program Files`, and add a `Start menu` shortcut to the executable that
    will start the program on login.

    *Note*: The install script will prompt for elevation if you haven't already
    opened the PowerShell terminal as an administrator.

## TODO

- [ ] [App in main thread](https://github.com/rust-windowing/winit/issues/2900#issuecomment-1639241944)
- [ ] Run tray from app
