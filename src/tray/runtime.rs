use super::menu::MenuItems;
use crate::event::Event;
use crossbeam_channel::Receiver as TrayReceiver;
use flume::{Receiver, Sender};
use tokio::runtime::Runtime as AsyncRuntime;
use tray_icon::{
  menu::{Menu, MenuEvent},
  TrayIcon, TrayIconEvent,
};
use winit::event_loop::ControlFlow;
use winit::{event::Event as WindowEvent, event_loop::EventLoopWindowTarget};

pub struct TrayEventHandler<'a> {
  tx: Sender<Event>,
  rx: Receiver<Event>,
  #[allow(unused)]
  menu: Menu,
  #[allow(unused)]
  tray: TrayIcon,
  items: MenuItems,
  async_runtime: AsyncRuntime,
  menu_channel: &'a TrayReceiver<MenuEvent>,
  tray_channel: &'a TrayReceiver<TrayIconEvent>,
}

impl<'a> TrayEventHandler<'a> {
  pub fn new(
    tx: Sender<Event>,
    rx: Receiver<Event>,
    tray: TrayIcon,
    menu: Menu,
    items: MenuItems,
  ) -> Self {
    Self {
      tx,
      rx,
      tray,
      menu,
      items,
      async_runtime: tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap(),
      menu_channel: MenuEvent::receiver(),
      tray_channel: TrayIconEvent::receiver(),
    }
  }

  pub fn handle_event<T>(
    &self,
    _event: WindowEvent<'a, T>,
    _target: &EventLoopWindowTarget<T>,
    control_flow: &mut ControlFlow,
  ) {
    *control_flow = ControlFlow::Poll;

    if let Ok(event) = self.rx.try_recv() {
      match event {
        Event::Quit => {
          *control_flow = ControlFlow::Exit;
          return;
        }
        _ => (),
      }
    }

    if let Ok(event) = self.menu_channel.try_recv() {
      log::debug!("Tray menu event: {:?}", event);
      if event.id == self.items.quit.id() {
        self.send_event(Event::Quit)
      }
    }

    if let Ok(event) = self.tray_channel.try_recv() {
      log::debug!("Tray icon event: {:?}", event);
    }
  }

  fn send_event(&self, event: Event) {
    let tx = self.tx.clone();
    self
      .async_runtime
      .spawn(async move { tx.send_async(event).await });
  }
}
