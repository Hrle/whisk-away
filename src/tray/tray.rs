use tray_icon::{icon::Icon, menu::Menu, TrayIcon, TrayIconBuilder};

#[cfg(target_os = "linux")]
pub fn create(icon: Icon, menu: Menu) -> TrayIcon {
  let (tx, rx) = flume::bounded(1);
  std::thread::spawn(move || {
    gtk::init().unwrap();
    let tray = build(menu, icon);
    tx.send(tray.clone());
    gtk::main();
  });
  rx.recv().unwrap()
}

#[cfg(not(target_os = "linux"))]
pub fn create(icon: Icon, menu: Menu) -> TrayIcon {
  build(menu, icon)
}

fn build(menu: Menu, icon: Icon) -> TrayIcon {
  TrayIconBuilder::new()
    .with_menu(Box::new(menu))
    .with_icon(icon)
    .build()
    .unwrap()
}
