use tray_icon::menu::{Menu, MenuItem};

#[derive(Clone)]
pub struct MenuItems {
  pub quit: MenuItem,
}

#[derive(Clone)]
pub struct MenuWithItems {
  pub menu: Menu,
  pub items: MenuItems,
}

pub fn create() -> MenuWithItems {
  let menu = Menu::new();
  let quit = MenuItem::new("Quit", true, None);
  menu.append_items(&[&quit]).unwrap();

  MenuWithItems {
    menu: menu,
    items: { MenuItems { quit: quit } },
  }
}
