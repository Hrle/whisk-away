use self::{menu::MenuWithItems, runtime::TrayEventHandler};
use crate::{config::Config, event::Event};
use flume::{Receiver, Sender};
use std::env;
use winit::event_loop::EventLoopBuilder;

mod icon;
mod menu;
mod runtime;
mod tray;

pub fn run(tx: Sender<Event>, rx: Receiver<Event>, _config: Config) {
  let icon = icon::load(
    env::current_exe()
      .unwrap()
      .parent()
      .unwrap()
      .join("assets")
      .join("logo-transparent-16x16.png")
      .as_path(),
  );
  let MenuWithItems { menu, items } = menu::create();
  let tray = tray::create(icon.clone(), menu.clone());

  let event_handler = runtime::TrayEventHandler::new(
    tx,
    rx,
    tray.clone(),
    menu.clone(),
    items.clone(),
  );

  start_event_loop(event_handler);
}

#[cfg(not(target_os = "windows"))]
fn start_event_loop(event_handler: TrayEventHandler<'static>) {
  EventLoopBuilder::new()
    .build()
    .run(move |event, target, control_flow| {
      event_handler.handle_event(event, target, control_flow)
    });
}

#[cfg(target_os = "windows")]
fn start_event_loop(event_handler: TrayEventHandler<'static>) {
  use winit::platform::windows::EventLoopBuilderExtWindows;
  EventLoopBuilder::new().with_any_thread(true).build().run(
    move |event, target, control_flow| {
      event_handler.handle_event(event, target, control_flow)
    },
  );
}
