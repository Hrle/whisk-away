use crate::{config::Config, event::Event};
use flume::{Receiver, Sender};

#[derive(Debug)]
pub struct Flags {
  pub config: Config,
  pub rx: Receiver<Event>,
  pub tx: Sender<Event>,
}
