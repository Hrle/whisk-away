use super::event::EventSubscription;
use super::flags::Flags;
use super::message::Message;
use crate::config::Config;
use crate::event::Event;
use flume::{Receiver, Sender};
use iced::widget::{button, column};
use iced::{executor, window, Alignment};
use iced::{Application, Command, Element, Subscription, Theme};

enum AppState {
  Running,
}

pub struct App {
  #[allow(unused)]
  config: Config,
  tx: Sender<Event>,
  rx: Receiver<Event>,
  #[allow(unused)]
  state: AppState,
}

impl Application for App {
  type Executor = executor::Default;
  type Flags = Flags;
  type Message = Message;
  type Theme = Theme;

  fn new(flags: Self::Flags) -> (Self, Command<Self::Message>) {
    (
      Self {
        config: flags.config.clone(),
        tx: flags.tx.clone(),
        rx: flags.rx.clone(),
        state: AppState::Running,
      },
      Command::none(),
    )
  }

  fn title(&self) -> String {
    String::from("Whisk Away")
  }

  fn subscription(&self) -> Subscription<Self::Message> {
    Subscription::from_recipe(EventSubscription {
      rx: self.rx.clone(),
    })
    .map(Message::Event)
  }

  fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
    match message {
      Message::Event(event) => match event {
        Event::Quit => window::close(),
        _ => Command::none(),
      },
      Message::Quit => self.send_event(Event::Quit),
    }
  }

  fn view(&self) -> Element<Self::Message> {
    column![button("Quit").on_press(Message::Quit)]
      .padding(20)
      .align_items(Alignment::Center)
      .into()
  }
}

impl App {
  fn send_event(&self, event: Event) -> Command<<App as Application>::Message> {
    let future_tx = self.tx.clone();
    let future_event = event.clone();
    Command::perform(
      async move { future_tx.send_async(future_event).await },
      move |result| {
        if let Err(e) = result {
          log::error!("Error sending quit event: {}", e);
        }

        Message::Event(event)
      },
    )
  }
}
