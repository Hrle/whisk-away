use self::app::App;
use self::flags::Flags;
use crate::config::Config;
use crate::event::Event;
use flume::{Receiver, Sender};
use iced::{Application, Settings};

mod app;
mod event;
mod flags;
mod message;

pub fn run(tx: Sender<Event>, rx: Receiver<Event>, config: Config) {
  let settings = Settings {
    antialiasing: true,
    ..Settings::<Flags>::with_flags(Flags { rx, tx, config })
  };
  if let Err(e) = App::run(settings) {
    log::error!("Error running app: {}", e);
  }
}
