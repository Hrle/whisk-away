use crate::event::Event;
use flume::Receiver;
use iced::futures;
use std::hash::Hash;

pub struct EventSubscription {
  pub rx: Receiver<Event>,
}

impl<H, I> iced::subscription::Recipe<H, I> for EventSubscription
where
  H: std::hash::Hasher,
{
  type Output = Event;

  fn hash(&self, state: &mut H) {
    std::any::TypeId::of::<Self>().hash(state);
    "event".hash(state);
  }

  fn stream(
    self: Box<Self>,
    _input: futures::stream::BoxStream<'static, I>,
  ) -> futures::stream::BoxStream<'static, Self::Output> {
    let poll_rx = self.rx.clone();
    Box::pin(futures::stream::poll_fn(move |_| {
      if let Ok(event) = poll_rx.try_recv() {
        futures::task::Poll::Ready(Some(event))
      } else {
        futures::task::Poll::Pending
      }
    }))
  }
}
