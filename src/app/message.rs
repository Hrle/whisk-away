use crate::event::Event;

#[derive(Debug, Clone)]
pub enum Message {
  Quit,
  Event(Event),
}
