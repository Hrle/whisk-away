use env_logger::{Builder, Target};
use std::fs::File;

pub fn init() {
  let log_path = dirs::data_dir()
    .unwrap()
    .join("whisk-away")
    .join("whisk-away.log");
  let target = Box::new(File::create(log_path).expect("Can't create file"));

  let mut builder = Builder::new();
  if cfg!(debug_assertions) {
    builder.filter_level(log::LevelFilter::Debug);
  } else {
    builder.filter_level(log::LevelFilter::Info);
  }

  builder
    .parse_env("WHISK_AWAY_LOG")
    .target(Target::Pipe(target))
    .init();
}
