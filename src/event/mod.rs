pub mod demultiplexer;

#[derive(Debug, Clone)]
pub enum Event {
  Quit,
  #[allow(unused)]
  Other,
}
