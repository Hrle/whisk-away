use flume::{Receiver, Sender};

use super::Event;

pub struct EventDemux<const N: usize> {
  pub tx: Sender<Event>,
  rx: Receiver<Event>,
  txs: [Sender<Event>; N],
  pub rxs: [Receiver<Event>; N],
}

impl<const N: usize> EventDemux<N> {
  pub fn new() -> Self {
    let (tx, rx) = flume::unbounded::<Event>();
    let channels = [(); N].map(|_| flume::unbounded());
    Self {
      tx,
      rx,
      txs: channels.clone().map(|(tx, _)| tx),
      rxs: channels.clone().map(|(_, rx)| rx),
    }
  }

  pub fn run(&self) {
    for event in self.rx.iter() {
      for tx in self.txs.iter() {
        tx.send(event.clone()).unwrap();
      }

      if let Event::Quit = event {
        break;
      }
    }
  }
}
