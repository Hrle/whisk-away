use crate::config::Config;
use crate::event::Event;
use flume::{Receiver, RecvTimeoutError, Sender};
use notify::{
  event::CreateKind, recommended_watcher, EventKind, RecursiveMode, Watcher,
};
use std::fs;
use std::io::Error;
use std::path::{Path, PathBuf};

pub fn watch(_tx: Sender<Event>, rx: Receiver<Event>, config: Config) {
  let home = dirs::home_dir().unwrap();

  let (watch_tx, watch_rx) = flume::unbounded();

  let mut watcher = recommended_watcher(move |result| {
    watch_tx.send(result).unwrap();
  })
  .unwrap();

  watcher.watch(&home, RecursiveMode::Recursive).unwrap();

  loop {
    if let Ok(event) = rx.try_recv() {
      match event {
        Event::Quit => {
          log::info!("Quit event received");
          break;
        }
        _ => (),
      }
    }

    match watch_rx.recv_timeout(std::time::Duration::from_millis(100)) {
      Ok(result) => match result {
        Ok(event) => match event.kind {
          EventKind::Create(kind) => match kind {
            CreateKind::File | CreateKind::Folder | CreateKind::Any => {
              for path in &event.paths {
                if let Some(extension) = path.extension() {
                  log::debug!("Extension: {:?}", extension);
                  if let Some(destination) =
                    config.associations.get(extension.to_str().unwrap())
                  {
                    log::debug!("Destination: {:?}", destination);
                    if let Err(e) = r#move(path, Path::new(destination)) {
                      log::info!("File moving error: {:?}", e);
                    }
                  }
                }
              }
            }
            _ => (),
          },
          _ => (),
        },
        Err(e) => log::error!("watch error: {:?}", e),
      },
      Err(e) => match e {
        RecvTimeoutError::Timeout => (),
        RecvTimeoutError::Disconnected => {
          log::error!("Watch disconnected")
        }
      },
    }
  }
}

fn r#move(path: &PathBuf, destination: &Path) -> Result<(), Error> {
  let filename = path.file_name().unwrap();
  let new_location = destination.join(filename);

  if let Some(parent) = new_location.parent() {
    if !parent.exists() {
      fs::create_dir_all(parent)?;
    }
  }

  fs::rename(path, new_location)?;

  Ok(())
}
