#![windows_subsystem = "windows"]

use config::Config;
use event::demultiplexer::EventDemux;

mod app;
mod config;
mod event;
mod file;
mod log;
mod tray;

fn main() {
  log::init();
  let config = Config::new();

  let event_demux = EventDemux::<3>::new();

  let ctrlc_tx = event_demux.tx.clone();
  ctrlc::set_handler(move || {
    ctrlc_tx.send(event::Event::Quit).unwrap();
  })
  .expect("Error setting SIGTERM handler");

  let file_tx = event_demux.tx.clone();
  let file_rx = event_demux.rxs[0].clone();
  let file_config = config.clone();
  let file_handle = std::thread::spawn(move || {
    file::watch(file_tx, file_rx, file_config);
  });

  let tray_tx = event_demux.tx.clone();
  let tray_rx = event_demux.rxs[1].clone();
  let tray_config = config.clone();
  let tray_handle = std::thread::spawn(move || {
    tray::run(tray_tx, tray_rx, tray_config);
  });

  let app_tx = event_demux.tx.clone();
  let app_rx = event_demux.rxs[2].clone();
  let app_config = config.clone();
  let app_handle = std::thread::spawn(move || {
    app::run(app_tx, app_rx, app_config);
  });

  let demux_handle = std::thread::spawn(move || {
    event_demux.run();
  });

  file_handle.join().unwrap();
  app_handle.join().unwrap();
  tray_handle.join().unwrap();
  demux_handle.join().unwrap();
}
