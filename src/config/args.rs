use clap::Parser;

#[derive(Parser, Debug, Clone)]
#[command(author, version, about, long_about = None)]
pub struct Args {}

impl Args {
  pub fn new() -> Self {
    Self::parse()
  }
}
