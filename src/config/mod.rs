mod args;

use args::Args;
use dirs::config_dir;
use std::collections::HashMap;
use std::fs::read_to_string;
use toml::Value;

#[derive(Debug, Clone)]
pub struct Config {
  pub args: Args,
  pub associations: HashMap<String, String>,
}

impl Config {
  pub fn new() -> Self {
    let args = Args::new();

    let mut config_path = config_dir().unwrap();
    config_path.push("whisk-away/config.toml");

    let mut associations = HashMap::new();

    if let Ok(contents) = read_to_string(&config_path) {
      if let Ok(config) = contents.parse::<Value>() {
        if let Some(table) =
          config.get("associations").and_then(Value::as_table)
        {
          for (extension, v) in table {
            if let Some(destination) = v.as_str() {
              if let Ok(pointer) = shellexpand::full(destination) {
                associations.insert(extension.to_string(), pointer.to_string());
              }
            }
          }
        }
      }
    }

    Self { args, associations }
  }
}
